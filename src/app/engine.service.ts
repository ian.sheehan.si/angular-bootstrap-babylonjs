import {ElementRef, Injectable, NgZone} from '@angular/core';

import * as BABYLON from "@babylonjs/core";
import "@babylonjs/loaders/glTF"; // npm install --save-dev @babylonjs/core @babylonjs/loaders
import "@babylonjs/core/Debug/debugLayer";
import '@babylonjs/inspector';
import Ammo from 'ammojs-typed';
import {AbstractMesh} from "@babylonjs/core";

@Injectable({
  providedIn: 'root'
})

export class EngineService {


  private canvas: HTMLCanvasElement;
  private engine: BABYLON.Engine;
  private scene: BABYLON.Scene;
  private light: BABYLON.Light;
  private deviceSourceManager: BABYLON.DeviceSourceManager;

  // private camera: BABYLON.FreeCamera;
  public camera: BABYLON.ArcRotateCamera;
  public cameraDistance = 7.5;
  public pushForce = 0.5;
  public angularDrag = 0.95;
  public player: BABYLON.AbstractMesh;
  public playerEmpty: BABYLON.AbstractMesh;
  // private Ammo: typeof AmmoModule; // ena možnost

  // KEYBOARD STUFF

  // lahko bi bil objekt večih stanjk kam naj se gre
  private forward = false;
  private backward = false;
  private left = false;
  private right = false;


  public sceneLoadedFinish = false;
  // private keyMap = new Map<number, String>();


  // public ammo = require('ammo.js/builds/ammo.js');

  // OSTALO
  // private sphere: BABYLON.Mesh;


  public constructor(
    private ngZone: NgZone,
    // private windowRef: WindowRefService
    // private ammo: Ammo // to ne gre ne dela
  ) {
  }

  // predelal z (https://playground.babylonjs.com/)
  public async createScene(canvas: ElementRef<HTMLCanvasElement>) { // : void
    this.canvas = canvas.nativeElement;

    // This creates a basic Babylon Scene object (non-mesh)
    this.engine = new BABYLON.Engine(this.canvas, true);
  }
}
