import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
// import { EngineService } from './engine.service';
import * as BABYLON from 'babylonjs';
// import 'babylonjs-loaders';



import Ammo from 'ammojs-typed';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'angular-bootstrap-babylonjs';

  @ViewChild('renderCanvas', { static: true })
  public renderCanvas!: ElementRef<HTMLCanvasElement>;

  private _engine!: BABYLON.Engine;
  private _scene!: BABYLON.Scene;
  private _light: any;

  public _camera!: BABYLON.ArcRotateCamera;
  public player!: BABYLON.AbstractMesh;
  public playerEmpty!: BABYLON.AbstractMesh;
  public _cameraDistance = 7.5;
  public _sceneLoadedFinish!: boolean;

  // public camera!: BABYLON.ArcRotateCamera;

  public constructor(
    private elementRef: ElementRef,
    // private engServ: EngineService
  ) {
    //
    this.renderCanvas = elementRef.nativeElement;
    console.log(this.renderCanvas);
  }

  public async ngOnInit(): Promise<void> {
    // console.log("renderCanvas: ", this.rendererCanvas)
    // this.engServ.createScene(this.rendererCanvas);
    // this.engServ.animate();

    this._engine = new BABYLON.Engine(this.renderCanvas.nativeElement, true);
    this._scene = new BABYLON.Scene(this._engine);
    this._scene.debugLayer.show();

    this._scene.clearColor = new BABYLON.Color4(0.0, 0.0, 0.0, 1);

    //--------------------

    this._camera = new BABYLON.ArcRotateCamera(
      "ArcRotateCamera",
      -Math.PI / 4, (Math.PI / 3),
      this._cameraDistance,
      BABYLON.Vector3.Zero(),
      this._scene );

    this._light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 0), this._scene);
    this._light.intensity = 0.7;
    const gravityVector = new BABYLON.Vector3(0, -10, 0);

    // const ammoAPI = await Ammo.bind(window)();
    // const physicsPlugin = new BABYLON.AmmoJSPlugin();

    // const ammoAPI = await Ammo.bind(window)();
    // (async () => {
      // const ammoAPI = await Ammo.bind(window)();
      const ammoAPI =  await this.getAmmoAPI();
      // use ammoAPI here

      const physicsPlugin = new BABYLON.AmmoJSPlugin(true, ammoAPI);
      this._scene.enablePhysics(new BABYLON.Vector3(0, -9.81, 0), physicsPlugin);

      //--------------------

      console.log("-> START LOADING SCENE");
      // await this.loadMyScene(this.scene);
      this.loadMyScene(this._scene);
      console.log("<- END LOADING SCENE (CHECK ASYNC OR SYNC");


      this._engine.runRenderLoop(() => {

        // if (this._sceneLoadedFinish)
        if (this._sceneLoadedFinish === true) {
          this._scene.render();
        }

      });
      window.addEventListener('resize', () => {
        this._engine.resize();
      });

    // })();



  } // END ngOnInit


  async getAmmoAPI(): Promise<any> {
    const ammoAPI = await Ammo.bind(window)();
    return ammoAPI;
  }



  public loadMyScene(inScene: any) {

    // , await ( (scene) => {
    // BABYLON.SceneLoader.Append("assets/Levels/", "level_v1.gltf", inScene, (scene) => {
    // VEDNO LOAD-AJ .glb file !!!
    // BABYLON.SceneLoader.Append("assets/Levels/", "level_v1.glb", inScene, (scene) => {
    BABYLON.SceneLoader.Append(
      "assets/Levels/",
      "level_v1.gltf",
      inScene,
      (
        scene
        //meshes

      ) => {
        console.log(scene);
      //console.log("You Load .gltf file scene: ", scene.meshes);

      /*
      const meshes = scene.meshes; // : BABYLON.AbstractMesh[]
      // for (const it in meshes) {
      for (let i = 0; i < meshes.length; i++) {
        const meshIt = meshes[i];
        // dodaj Colliders (SphereCollider, BoxCollider)
        if (meshIt.name === "Player_with_sphere_collider") {
          console.log("SphareCollider - Dynamic (Player) : ", meshIt.name);
          this.player = meshIt;
          this._camera.setTarget(this.player.position)
          this.createPhysicsImpostor(this._scene, meshIt, BABYLON.PhysicsImpostor.SphereImpostor, {
            mass: 1.0,
            friction: 1.0,
            restitution: 1.5
          }, true);
          this.playerEmpty = BABYLON.Mesh.CreateBox("box", 1.6, scene);
          this.playerEmpty.visibility = 0;
          this.playerEmpty.position = this.player.position; // playerEmpty-ju je starš this.player (this.player = je starš, this.playerEmpty = child od this.player)

          // this.playerEmpty.rotation = this.player.rotation;
          //this.playerEmpty.rotationQuaternion = this.playerEmpty.rotationQuaternion;

          // this.playerEmpty.parent = this.player;

          // this.player.rotation = this.playerEmpty.rotation;
          // this.playerEmpty.rotation = this.player.rotation;
          // this.playerEmpty.rotationQuaternion = this.playerEmpty.rotationQuaternion);

        } else if (meshIt.name.indexOf("_with_sphere_collider") !== -1) {
          console.log("SphareCollider - Static: ", meshIt.name);
          // this.createPhysicsImpostor(scene, mesh, BABYLON.PhysicsImpostor.SphereImpostor, { mass: 0, friction: 0.5, restitution: 0 }, true);
        } else if (meshIt.name.indexOf("_with_box_collider") !== -1) {
          console.log("BoxCollier - Static: ", meshIt.name);
          this.createPhysicsImpostor(scene, meshIt, BABYLON.PhysicsImpostor.BoxImpostor, {
            mass: 0,
            friction: 1.0,
            restitution: 0
          }, true);
        } else if (meshIt.name.indexOf("_with_mesh_collider") !== -1) {
          console.log("MeshCollier - Static: ", meshIt.name);
          this.createPhysicsImpostor(scene, meshIt,
            BABYLON.PhysicsImpostor.MeshImpostor,
            {
              mass: 0,
              friction: 1.0,
              restitution: 0
            },true);
        } else if (meshIt.name.indexOf("_with_convex_hull_collider") !== -1) {
          console.log("MeshCollier - Static: ", meshIt.name);
          this.createPhysicsImpostor(scene, meshIt,
            BABYLON.PhysicsImpostor.ConvexHullImpostor,
            {
              mass: 0,
              friction: 1.0,
              restitution: 0
            },true);
        }
        */
        /*
        this.createPhysicsImpostor(this.scene, mesh, BABYLON.PhysicsImpostor.BoxImpostor, {
          mass: 1,
          restitution: 0.9
        }, true);
        */

      // } // END FOR

      //this._sceneLoadedFinish = true;

    }); // END loadMyScene


  }

  public createPhysicsImpostor(scene: any, entity: any, impostor: any, options: any, reparent: any) {
    if (entity == null)
      return;
    entity.checkCollisions = false;
    const parent = entity.parent;
    if (reparent === true)
      entity.parent = null;
    entity.physicsImpostor = new BABYLON.PhysicsImpostor(entity, impostor, options, scene);
    // NIMAM POJMA KAJ TA DEL KOMENTIRANE KODE BIL SPLOH MIKŠLJEN s strani #MackeyK24:
    // https://forum.babylonjs.com/u/mackeyk24/summary
    // na forumu tukaj: https://forum.babylonjs.com/t/loading-gltf-and-physics-not-working-what-am-i-missing/4878
    // if (reparent === true)
    // entity.parent = parent;
  };


  public async createScene() {

  }


}
